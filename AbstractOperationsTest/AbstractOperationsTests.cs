﻿using System;
using System.Collections.Generic;
using System.Linq;
using AbstractOperations;
using NUnit.Framework;

namespace AbstractOperationsTest
{
    [TestFixture]
    public class AbstractOperationsTests
    {
        private IAbstractOperation<int> GetOperation(IEnumerable<OperationStateDelegate<int>> operations)
        {
            return new AbstractOperation<int>(operations);
        }

        private static IAbstractOperation<int> GetOperation(IEnumerable<IAbstractOperation<int>> operations)
        {
            return new AbstractOperation<int>(operations);
        }

        [Test]
        public void Run_RunsEveryStepInOrder_AsExpected()
        {
            //Result of steps will (N + 2) * 2;
            var steps = new List<OperationStateDelegate<int>>
            {
                state =>
                {
                    state.Value += 2;
                    return state;
                },
                state =>
                {
                    state.Value *= 2;
                    return state;
                }
            };
            var operation = GetOperation(steps);

            //N == 2;
            var resultState = operation.Run(new OperationState<int>(2));

            //(2+2) * 2 == 8;
            Assert.AreEqual(8,resultState.Value);
        }

        [Test]
        public void Run_OneOfStepAddError_HaveErrorMustBeTrueAndErrorCountMoreThanZero()
        {
            // Two steps do increment and one (in midle) add error
            var steps = new List<OperationStateDelegate<int>>
            {
                state =>
                {
                    state.Value++;
                    return state;
                },
                state =>
                {
                    state.AddError(new Exception());
                    return state;
                },
                state =>
                {
                    state.Value++;
                    return state;
                }
            };
            var operation = GetOperation(steps);


            var resultState = operation.Run(new OperationState<int>());

            var valueEquals2 = resultState.Value == 2;
            var haveErrors = resultState.HaveErrors;
            var errorCountIs1 = resultState.Errors.Count() == 1;
            Assert.IsTrue(valueEquals2 && haveErrors && errorCountIs1);
        }

        [Test]
        public void Run_OneOfStepThrowsException_HaveErrorMustBeTrueAndErrorCountMoreThanZero()
        {
            // Two steps do increment and one (in midle) add error
            var steps = new List<OperationStateDelegate<int>>
            {
                state =>
                {
                    state.Value++;
                    return state;
                },
                state =>
                {
                    throw new Exception();
                },
                state =>
                {
                    state.Value++;
                    return state;
                }
            };
            var operation = GetOperation(steps);


            var resultState = operation.Run(new OperationState<int>());

            var valueEquals2 = resultState.Value == 2;
            var haveErrors = resultState.HaveErrors;
            var errorCountIs1 = resultState.Errors.Count() == 1;
            Assert.IsTrue(valueEquals2 && haveErrors && errorCountIs1);
        }

        [Test]
        public void RunAsync_OneOfStepThrowsException_HaveErrorMustBeTrueAndErrorCountMoreThanZero()
        {
            // Two steps do increment and one (in midle) add error
            var steps = new List<OperationStateDelegate<int>>
            {
                state =>
                {
                    state.Value++;
                    return state;
                },
                state =>
                {
                    throw new Exception();
                },
                state =>
                {
                    state.Value++;
                    return state;
                }
            };
            var operation = GetOperation(steps);


            var task = operation.RunAsync(new OperationState<int>());
            task.Wait();
            var resultState = task.Result;

            var valueEquals2 = resultState.Value == 2;
            var haveErrors = resultState.HaveErrors;
            var errorCountIs1 = resultState.Errors.Count() == 1;
            Assert.IsTrue(valueEquals2 && haveErrors && errorCountIs1);
        }

        [Test]
        public void RunAsync_JoinsTwoOperationWhereTwoOfStepThrowsException_HaveErrorMustBeTrueAndErrorCountMoreThanZero()
        {
            // Two steps do increment and one (in midle) add error
            var steps = new List<OperationStateDelegate<int>>
            {
                state =>
                {
                    state.Value++;
                    return state;
                },
                state =>
                {
                    throw new Exception();
                },
                state =>
                {
                    state.Value++;
                    return state;
                }
            };
            var operation = GetOperation(steps);
            var operation2 = GetOperation(steps);
            var twoOperations = GetOperation(new []{operation,operation2}.AsEnumerable());

            var task = twoOperations.RunAsync(new OperationState<int>());
            task.Wait();
            var resultState = task.Result;

            var valueEquals4 = resultState.Value == 4;
            var haveErrors = resultState.HaveErrors;
            var errorCountIs2 = resultState.Errors.Count() == 2;
            Assert.IsTrue(valueEquals4 && haveErrors && errorCountIs2);
        }

    }
}
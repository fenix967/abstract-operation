﻿using System.Threading.Tasks;

namespace AbstractOperations
{
    public interface IAbstractOperation<T>
    {
        IOperationState<T> Run(IOperationState<T> initialState);
        Task<IOperationState<T>> RunAsync(IOperationState<T> initialState);
    }
}
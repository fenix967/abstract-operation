﻿using System;
using System.Collections.Generic;

namespace AbstractOperations
{
    public interface IOperationState<T>
    {
        T Value { get; set; }
        IEnumerable<Exception> Errors { get; }
        bool HaveErrors { get; }
        void AddError(Exception error);
    }
}
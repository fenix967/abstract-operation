﻿namespace AbstractOperations
{
    public delegate IOperationState<T> OperationStateDelegate<T>(IOperationState<T> state);
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AbstractOperations
{
    public class AbstractOperation<T> : IAbstractOperation<T>
    {
        private readonly List<OperationStateDelegate<T>> _opList;

        public AbstractOperation(IEnumerable<OperationStateDelegate<T>> operationDelegates)
        {
            _opList = new List<OperationStateDelegate<T>>(operationDelegates);
        }

        public AbstractOperation(IEnumerable<IAbstractOperation<T>> operations)
        {
            _opList = operations.Select(o => (OperationStateDelegate<T>)o.Run).ToList();
        }

        public IOperationState<T> Run(IOperationState<T> initialState)
        {
            return _opList.Aggregate(initialState, (current, step) =>
            {
                var state = current;
                try
                {
                    state = step(state);
                }
                catch (Exception error)
                {
                    state.AddError(error);
                }
                return state;
            });
        }

        public async Task<IOperationState<T>> RunAsync(IOperationState<T> initialState)
        {
            var stepState = initialState;

            foreach (var step in _opList)
            {
                try
                {
                    stepState = await DoAsync(step, stepState);
                }
                catch (Exception error)
                {
                    stepState.AddError(error);
                }
            }

            return stepState;
        }

        private static async Task<IOperationState<T>> DoAsync(OperationStateDelegate<T> step, IOperationState<T> state)
        {
           return await Task.Run(()=>step(state));
        }
    }
}
﻿using System;
using System.Collections.Generic;

namespace AbstractOperations
{
    public class OperationState<T> : IOperationState<T>
    {
        public OperationState()
        {
        }

        public OperationState(IEnumerable<Exception> errors)
        {
            _errors.AddRange(errors);
        }

        public OperationState(T value,IEnumerable<Exception>errors)
        {
            Value = value;
            _errors.AddRange(errors);
        }
        private readonly List<Exception> _errors = new List<Exception>();

        public OperationState(T value)
        {
            Value = value;
        }

        public T Value { get; set; }

        public IEnumerable<Exception> Errors => _errors;

        public bool HaveErrors => _errors.Count > 0;

        public void AddError(Exception error)
        {
            _errors.Add(error);
        }
    }
}